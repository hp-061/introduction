
## INTRODUCTION


### My Biography


#### Here is some short info about me 

<img src="https://gitlab.com/hp-061/introduction/-/raw/main/hp/WhatsApp_Image_2021-10-28_at_22.45.44.jpeg" width=150 align=middle>

* *NAME:* NURUL HANIS BINTI ZAWAWI

* *AGE:* 22

* *MATRIC NO:* 197542

* *COURSE:* Bachelor Of Aerospace Engineering  With Honors

## Strength & Weakness

| Strength | Weakness |
| :----------: | :-----------: |
| Positive | Impatience |
| Extrovert | Stubborn | 
| Daring | Like to procrastinate |

