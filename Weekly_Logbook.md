##### **Name:** Nurul Hanis Binti Zawawi <br>  **Matric Number:** 197542 <br> **Subsystem:** Payload Design (Sprayer) <br> **Team:** Group 2

## Table of Content

[Week 1](#logbook-week-1) <br>
[Week 2](#logbook-week-2) <br>
[Week 3](#logbook-week-3) <br>
[Week 4](#logbook-week-4) <br>
[Week 5](#logbook-week-5) <br>
[Week 6](#logbook-week-6) <br>
[Week 7](#logbook-week-7) <br>
[Week 8](#logbook-week-8) <br>
[Week 9](#logbook-week-9) <br>
[Week 10](#logbook-week-10) <br>
[Week 11](#logbook-week-11) <br>
[Week 12](#logbook-week-12) <br>
[Week 13](#logbook-week-13) <br>
[Week 14](#logbook-week-14) <br>


<div align="center">

## **Logbook Week 1**

</div>

### Agenda and Goals

### Problem and decision taken

### Impact

### Next Step

<div align="center">

## **Logbook Week 2**

</div>

### Agenda and Goals

- Have briefing about the project and the milestone that we try to achive by the end of the semester
- Check the availibility of the component that will be used
- To be able to finish the given gitlab task and help the other team member

### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| Unsure about the availibility of component that they had survey on a few website | Contact and email 3 of the supplier to ask about the availibity and recieved the reply within 3 hours |

The reason why we need to be sure about the availibility of the component is because some of it need to be ship from the oversea and it will take more time for us to recieved it. That is why we need to make sure the product is not pre-order and ready to be ship once we place the order on the website.

### Impact

- Helps the other to quickly decide where do they have to buy the ready stock product (from which website or supplier)
- Can placed order earlier and recieved the item sooner so we are on the track with the actual timeline

### Next Step

- Helps my subsystem decide on the design and component that need to be used for the payload (sprayer)
- Contact Aerodyne and ask about the availibility to have a visit to the company to see the actual drone that they use for their agricultural activity 



<div align="center">

## **Logbook Week 3**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Do a survey and visit Aerodyne to see how does their drone and sprayer system works
- Conduct meeting to share the information that we gain from the visit at Aerodyne to other member who didn't join the visit
- Conduct meeting to discuss the design and component for the sprayer
- Search for the component that we will use for our sprayer

**Team (Group 2)**
- Appoint role for the member of the group based on the task given which is Checker, Process Monitor, Recorder, and Coordinator.
- Share and discuss the progress of each subsystem to the other group member

### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| Some of the team member cannot imagine how the system of the sprayer will look like and how the design of the sprayer will be | Arrange for a meeting with Aerodyne to show and gain more knowledge about agricultural drone so the information can be adapt and used in our project |
| A few member of the subsytem team cannot join the visit | Make an online meeting after the visit to share the knowledge that we gain to make sure everyone is on the same track with each other |

After the visit in Aerodyne, we get the insight on how the system of their agricultural sprayer works and we got an idea on how the design of our sprayer will look like. We also get to increased our knowledge about the agricultural drone setup during the visit which is good for the member of the team.

### Impact

- We can start the survey for component and reach out to supplier to make sure the product that we want are ready to be ship if needed
- All of the member for the subsystem team are on the right track and no one is left behind
- Can start do the parameters calculation after we have the rough idea on how much the weight of the payload, the size of the sprayer, size of the pump, type of nozzle that we will use, how much area we can cover and etc 

### Next Step

- Everyone will do their survey for the component needed which is tank, pump and nozzle and we will do a meeting to finalise which product we will use (Subsystem)
- Arrange a meeting and discuss with fiqri whether the design and component that we choose are suitable to be used for the project
- Do a schedule to rotate the member role for next week since I'm the Coordinator for this week (Team, Group 2)



<div align="center">

## **Logbook Week 4**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Discuss about the calculation for parameters of the sprayer 
- Conduct meeting to discuss the design and component for the sprayer (ongoing)
- Search for the component that we will use for our sprayer (ongoing)

**Team (Group 2)**
- As the Coordinator I need to make sure each person who's responsible for their role is doing their respective task
- Share and discuss the progress of each subsystem to the other group member

### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| Need to put the link for the component that we found online | Do a separate tracker for the sprayer team to use to put link of component we found online |

The tracker can be refer to easily by the team member since it is accessible for everyone. It is also easy for the team members to compare the component that they found online and to update the availibility of the product. 

### Impact

- Helps the others to do comparison such as function and price of the components 


### Next Step

- Need to do the drawing in solidwork or catia so that the whole class can see how does the design really look like
- Confirm with Fiqri which component we will need to use so we can placed order quicker




<div align="center">

## **Logbook Week 5**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Conduct meeting to discuss the final design and component for the sprayer with Aleesya
- Discuss with Design team on how do we attach our mounting to their gondola

**Team (Group 2)**
- Remind the other team member on their role for that week since I'm the coordinator of the group 
- Share any update from each of the subsytem in the group chat 

### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| Quite a handful task that need to be done by the sprayer team | Divided the team member into few sub-team within sprayer subsystem to ease the job and make sure everyone contribute |
| Other can't really see or imagine how our design is in 2D sketches | Make a 3D drawing using Solidwork to make it easier to visualised during the presentation |
| Have a problem on how to attach our sprayer mounting to the gondola | Conduct a meeting with the design team so we can exchange idea and share our thoughts on how to attach the sprayer to the gondola |

Since there's a few task that need to be done within the short period, we divided the task within ourselves to make sure we manage to finished it in time. other than that, with the help from haikal, we managed to come out with the 3D drawing of the sprayer. We also managed to solve the problem with the design team on how to attach our sprayer to the gondola and airship. 

### Impact

- Task for this week being done smoothly for our subsytem
- All of the member for the subsystem team are on the right track and no one is left behind
- The whole class can see the visualised version of our 2D sketch of the sprayer mounting

### Next Step

- Start buying things for the mounting and sprayer
- Maybe can start assembling the component
- Make sure each person who have role for that week are doing the task they are assign to (Team)





<div align="center">

## **Logbook Week 6**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Discuss the pros and cons of the component that we will buy
- Send the finalised tracker to Fiqri so he can proceed with ordering the component
- Come out with the updated final weight of our payload (sprayer+mounting)

**Team (Group 2)**
- Make sure the Checker, Process Monitor and Recorder are doing the task that have been assign to them
- Share and discuss the progress of each subsystem to the other group member

### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| A few member of the Group 2 team member forgot to update their gitlab logbook | Remind them in the group chat every other day to update their gitlab |

Since some of us started to have test this week, maybe they are busy and forgot to update their gitlab so we remind them in the group chat to do it before friday.

### Impact

- Make sure everyone is on the right track and is not left behind

### Next Step

- Start assembling the mounting for the sprayer 




<div align="center">

## **Logbook Week 7**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Went out and buy the component from the store at Sri Serdang

**Team (Group 2)**
- Make sure the Checker, Process Monitor and Recorder are doing the task that have been assign to them
- Share and discuss the progress of each subsystem to the other group member

### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| Our progress for the sprayer subsytem are a bit slow this week | Do the assembling during mid sem break |
| A few member of the Group 2 team member forgot to update their gitlab logbook | Remind them in the group chat every other day to update their gitlab |

Since some of us started to have test this week, maybe they are busy and forgot to update their gitlab so we remind them in the group chat to do it before friday. 

### Impact

- Make sure everyone is on the right track and is not left behind

### Next Step

- Start assembling the mounting for the sprayer 




<div align="center">

## **Logbook Week 8**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Assembling the boomstick (using pvc and flextube)
- Discuss the new design for the sprayer because of the attachment issue

**Team (Group 2)**
- Make sure the Checker, Process Monitor and Recorder are doing the task that have been assign to them
- Remind the members to start doing the resume

### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| There is issue for the attachment of sprayer to the gondola | Come out with new ideas for the attachment of the sprayer to the airship |


Previously, the design of the sprayer is we will clamp the tank to the gondola and the gondola will be attached to the airship, but the gondola cannot withstand the weight of our payload so we come out with the idea of attaching the tank directly to the airship. The gondola will sit on top of the sprayer's tank. The new airship-payload attachment design need to meet the requirement listed:

- Battery accessibility
- Power board accessibility
- Stability
- Weight
- Thruster arm connector
- Attachment to airship location

### Impact

- Can sacknowledge and solve the problem faster so we didnt get side track from the current plan 

### Next Step

- Resolve pump and nozzle issue
- Start assembling the mounting for the sprayer 





<div align="center">

## **Logbook Week 9**

</div>

### Agenda and Goals

- No activities were held this week since the classes was postponed due to the flood and positive covid cases in the lab (20/12/2021 to 2/1/2022)


### Next Step

- Resolve pump and nozzle issue
- Sprayer test run





<div align="center">

## **Logbook Week 10**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Went to Poladrone to exchange the pump 
- Since the lab is closed, the pump testing and assembling cannot be done

**Team (Group 2)**
- No activities were held this week since the classes was postponed due to the flood and positive covid cases in the lab (20/12/2021 to 2/1/2022)


### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| The pump that we buy previously doesn't fit the criteria of pump that we need | Went to poladrone to exchange the pump to other type |
| Since the lab is closed, the progress will be a little bit slow | Re-arrange the schedule to make sure we are in the correct path until week 14 |

We went to exchange the pump but the lab is closed so we cannot start testing the new pump yet. 

### Impact

- Can start testing the pump and do the final assemble once the lab is open 

### Next Step

- Start pump and nozzle testing
- Assemble everything to the main tank 





<div align="center">

## **Logbook Week 11**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Pump and nozzle test
- Assemble everything to the tank

**Team (Group 2)**
- Make sure the Checker, Process Monitor and Recorder are doing the task that have been assign to them
- Remind the team members to update their engineering logbooks and to update their resume

### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| Leaking at T-connector | Tape the connector at the joint before connect it with flextube |
| Attachment for the pump to the tank | Using a wall plug so we dont have to cut open the tank to attach the pump and boomstick |

We do the pump testing to find out the flowrate of the pump. Next we test it using 2 nozzle before finally test using 4 nozzle. We also assemble everything together on a dummy pump first to check the durability and do the leak test first to find out if there is any other problem before drillig and asseble everything to the real tank.

### Impact

- Using the dummy tank first give us chance to test and try to find the best solution to attach the pump to the tank without causing a lot of damage to the tank
- It also gives us a glimpse on what might be a problem in the future once we use the real tank such as leakage problem at the attach part

### Next Step

- Drilling and attach the pump and boomstick to the real tank
- Battery duration test
- Find centre of gravity of whole system (FSI subsystem) 
- Pump switch control (Software and Control subsystem)







<div align="center">

## **Logbook Week 12**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Drilling and attach the pump and boomstick to the real tank
- Battery duration test
- Resolve any leaking problem

**Team (Group 2)**
- Make sure the Checker, Process Monitor and Recorder are doing the task that have been assign to them
- Remind the team members to update their engineering logbooks and to send the resume to FSI team.

### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| There's some leaking at the part where we attach the pump to the tank | WE use the waterproof tape before we screw in the screw |
| There water doesnt come out after the pump is attached to the tank | Change the position of the pump |

After drilling the hole, we insert the wall plug and screw in the pump but there is some leaking at the drilled part. As a solution, we use waterproff tape before we screw in the pump and the problem was solved. We use the same method to clamp the boomstick to the tank. When we were doing the sprayer test, we found out that the initial position of our pump is wrong because the water doesn't come out. After we change the position of the pump (180 degree), the pump is working. During the battery testing, for the first test, the battery used is 7% (1 minute 20 seconds) and for the second test, the battery used is 6% (1 minute 26 seconds)

### Impact

- The sprayer is working and are ready to be attach to the airship

### Next Step

- Pump switch control (Software and Control subsystem)
- Gondola attachment to tank 
- Atachment of whole system to airship





<div align="center">

## **Logbook Week 13**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Pump switch control (Software and Control subsystem)
- Gondola attachment to tank 

**Team (Group 2)**
- Make sure the Checker, Process Monitor and Recorder are doing the task that have been assign to them
- Remind the team members to update their engineering logbooks and to send the resume to FSI team.


### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| Gondola attachment to tank | Use a plate and a truss system to support the gondola |

The solution that we come out with to attach the gondola to the tank is using a plate on top of the tank and a truss system at the side that would support the gondola. A plywood is used as the plate that would support the gondola. 'L' bracket is added at the end of the tank to support the extended part of the gondola. Plywood is attached onto the tank using epoxy and double sided tape. 'L' bracket is screwed into the tank and then velcro strap is added onto the plate for gondola attachment.

### Impact

- The gondola can be placed on top of the tank

### Next Step

- Pump switch control (Software and Control subsystem)
- Flight test






<div align="center">

## **Logbook Week 14**

</div>

### Agenda and Goals

**Subsystem (Payload Design)**
- Pump switch control (Software and Control subsystem)
- Flight test

**Team (Group 2)**
- Make sure the Checker, Process Monitor and Recorder are doing the task that have been assign to them
- Remind the team members to update their engineering logbooks and to send the resume to FSI team.


### Problem and decision taken

| Problem | Solution |
| ------ | ------ |
| Switch to control on-off of the pump | Nava from the control team help to solve this problem using a relay switch and a wifi module|


We are using a relay switch and wifi module that is connected to the pump. An app is created for the on and off switch function. The pump can be turned on and off remotely through the app with the help of the wifi module.

### Impact

- Can control the pump switch directly from phone 

### Next Step

- Flight Test



